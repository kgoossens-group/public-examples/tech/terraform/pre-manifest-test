terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.16"
    }
  }
}
variable "gitlab_token" {}

provider "gitlab" {
  token = var.gitlab_token
}
