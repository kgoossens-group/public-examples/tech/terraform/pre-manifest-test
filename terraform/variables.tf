variable "username" {
  description = "The username for SSH access to the compute instance"
}

variable "gitlab_agent_project" {
  description = "The project we are using for the agent"
}

variable "gitlab_agent_group" {
  description = "The Group the agent gives access to"
}


variable "lbip" {
  type = string
  description = "external ingress ip"
}
