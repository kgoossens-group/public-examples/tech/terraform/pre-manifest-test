resource "gitlab_group_variable" "kube-ingress-base-domain" {
  group             = var.gitlab_agent_group
  key               = "KUBE_INGRESS_BASE_DOMAIN"
  value             = "${var.lbip}.nip.io"
  protected         = false
  masked            = false
  environment_scope = "*"
}

